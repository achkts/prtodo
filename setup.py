from setuptools import setup, find_packages

setup(
    name="prtodo",
    version="1.0",
    description="Create tasks in Todoist from new GitHub PRs",
    author="Achilleas Koutsou",
    author_email="achilleas.k@gmail.com",
    url="https://gitlab.com/achkts/prtodo",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "prtodo = prtodo.prtodo:main",
        ],
    },
    install_requires=("todoist-python>=8.1.1", "PyGithub>=1.47"),
)
