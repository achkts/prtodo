import os
import sys
from typing import Dict, List
import configparser

import github as gh
import todoist as td


TD_PRLABEL = "PR"


def _config_dir() -> str:
    homecfg = os.path.join(os.getenv("HOME", os.path.expanduser("~")),
                           ".config")
    homecfg = os.getenv("XDG_CONFIG_HOME", homecfg)
    return os.path.join(homecfg, "prtodo")


def read_config() -> configparser.ConfigParser:
    config = configparser.ConfigParser()
    config.read(os.path.join(_config_dir(), "config"))
    return config


def get_token(name: str) -> str:
    tokenfpath = os.path.join(_config_dir(), "tokens")
    prefix = f"{name}="
    try:
        with open(tokenfpath) as tf:
            for line in tf:
                line = line.strip()
                if line.startswith(prefix):
                    # dumb token parsing
                    return line[len(prefix):]
            else:
                print(f"Token for {name} not found in token file {tokenfpath}")
                sys.exit(1)

    except FileNotFoundError:
        print(f"Failed to load token for {name}: {tokenfpath} not found")
        sys.exit(1)
    return ""


def write_config(config: configparser.ConfigParser):
    with open(os.path.join(_config_dir(), "config"), "w") as cfile:
        config.write(cfile)


def init_github() -> gh.Github:
    """
    Initialises and returns a GitHub instance authenticated with the token
    from the file 'tokens' in the cwd.
    """
    token = get_token("github")
    return gh.Github(token)


def init_todoist() -> td.TodoistAPI:
    """
    Initialises and returns a TodoistAPI instance authenticated with the token
    from the file 'tokens' in the cwd.
    """
    token = get_token("todoist")
    return td.TodoistAPI(token)


def get_todoist_project(api: td.TodoistAPI, name: str):
    """
    Retrieves a project's information from Todoist.  If a project is not found
    with the provided name, the program exits with an error.
    """
    for project in api.state["projects"]:
        if project["name"].casefold() == name.casefold():
            return project
    print(f"Todoist: No project found with name {name}")
    sys.exit(1)


def get_todoist_prlabelid(tds: td.TodoistAPI) -> str:
    for label in tds["labels"]:
        if label["name"] == TD_PRLABEL:
            return label["id"]
    print(f"Todoist: no label found with name {TD_PRLABEL}")
    sys.exit(1)


def get_todoist_project_items(tds: td.TodoistAPI, projectid: str) -> List:
    projitems = list()
    for item in tds.items.get:
        if item["project_id"] == projectid:
            projitems.append(item)
    return projitems


def format_todo_item(pr: gh.PullRequest) -> str:
    title = pr.title
    number = pr.number
    author = pr.user.name
    if not author:
        author = pr.user.login
    url = pr.html_url
    return f"PR{number}: [{title}]({url}) by {author}"


def make_new_pr_todos(tds: td.TodoistAPI, projectname: str,
                      repo: gh.Repository):
    """
    Creates new Todoist items for each new Pull Request in the GitHub
    repository.
    """
    prlabelid = get_todoist_prlabelid(tds)
    project = get_todoist_project(tds, projectname)
    projectdata = tds.projects.get_data(project["id"])
    prtodos = [item["content"] for item in projectdata["items"]
               if prlabelid in item["labels"]]
    prtodonums = [item.split(":")[0] for item in prtodos]
    openprs: List[gh.PullRequest] = list(repo.get_pulls(state="open"))

    newprs = filter(lambda p: f"PR{p.number}" not in prtodonums, openprs)
    for newpr in newprs:
        newitemtext = format_todo_item(newpr)
        print(f"Adding new task\n\t{newitemtext}")
        tds.items.add(content=newitemtext,
                      project_id=project["id"],
                      date_string="next workday",
                      labels=[prlabelid])


def new_project():
    reponame = input("Name of GitHub repository: ")
    tdprojectname = input("Name of Todoist projet: ")
    # TODO: validate names
    config = read_config()
    if "projects" not in config:
        config["projects"] = {}
    config["projects"][reponame] = tdprojectname
    write_config(config)


def projects_cfg() -> Dict[str, str]:
    config = read_config()
    if "projects" in config:
        return dict(config["projects"])
    else:
        print("No projects configured")
        sys.exit(1)


def update():
    ghs = init_github()
    tds = init_todoist()
    tds.sync()
    for reponame, projectname in projects_cfg().items():
        repo = ghs.get_repo(reponame)
        make_new_pr_todos(tds, projectname, repo)
    tds.commit()


def main():
    commands = {
        "add-project": new_project,
        "update": update,
    }
    if len(sys.argv) < 2:
        print("Available commands:")
        print("\t" + "\n\t".join(commands))
        sys.exit(1)

    command = sys.argv[1]
    commands[command]()
