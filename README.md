# PR-Todo

Creates tasks in Todoist when new PRs are detected in watched repositories.  Relies on regular polling instead of hooks so it can be set up on any always-on machine without needing a publicly accessible server to be running or even admin access to the repository.
